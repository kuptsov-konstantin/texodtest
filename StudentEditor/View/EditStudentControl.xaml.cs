﻿using System;
using System.Linq;
using System.Windows.Controls;

namespace StudentEditor.View
{
    /// <summary>
    /// Логика взаимодействия для EditStudentControl.xaml
    /// </summary>
    public partial class EditStudentControl : UserControl
	{
		public EditStudentControl()
		{
			InitializeComponent();
		}
		public Boolean IsValid
		{
			get {
				var errorsFirstName = Validation.GetErrors(FirstName);
				var errorsLastName = Validation.GetErrors(LastName);

				return (errorsFirstName.Count == 0) && (errorsLastName.Count == 0);
			}
		}
		public string ValidationErrors
		{
			get
			{
				var errorsFirstName = Validation.GetErrors(FirstName);
				var errorsLastName = Validation.GetErrors(LastName);
				var errors = errorsFirstName.Select(error => error.ErrorContent).ToList();
				var allErrorsMessage = string.Join(", ", errors);
				errors = errorsLastName.Select(error => error.ErrorContent).ToList();
				return allErrorsMessage + ", " + string.Join(", ", errors);
			}
		}
	}
}

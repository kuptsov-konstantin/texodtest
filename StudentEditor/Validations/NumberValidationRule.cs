﻿using System.Windows.Controls;

namespace StudentEditor.Validations
{
    public class RequaredValidationRule : ValidationRule
	{
		public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
		{
			var str = value as string;
			return new ValidationResult(str.Length != 0, "Поле обязательно для заполнения");
		}
	}
}

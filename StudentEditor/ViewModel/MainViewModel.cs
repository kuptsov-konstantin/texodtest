﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Win32;
using StudentEditor.Extensions;
using StudentEditor.View;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Xml;
using System.Xml.Serialization;

namespace StudentEditor.ViewModel
{
    public class MainViewModel : ViewModelBase
	{
		private string _fileName = "";
		public string FileName
		{
			get => _fileName;
			set
			{
				_fileName = value;
				OnPropertyChanged("FileName");
			}
		}
		
		private bool _isLoaded = false;
		public bool IsLoaded
		{
			get => _isLoaded;
			set
			{
				_isLoaded = value;
				OnPropertyChanged("IsLoaded");
			}
		}

        private ObservableCollection<StudentViewModel> _workspaces;
		public ObservableCollection<StudentViewModel> Workspaces
		{
			get
			{
				if (_workspaces == null)
				{
					_workspaces = new ObservableCollection<StudentViewModel>();
				}
				return _workspaces;
			}
			set
			{
				_workspaces = value;
				OnPropertyChanged("Workspaces");
			}
		}

        private StudentViewModel _editorModel;
		public StudentViewModel EditorModel
		{
			get => _editorModel;
			set
			{
				_editorModel = value;
				OnPropertyChanged("EditorModel");
			}
		}


		private DelegateCommand<EditStudentControl> _addItemCommand;
		public ICommand AddItemCommand
		{
			get
			{
				if (_addItemCommand == null)
				{
					_addItemCommand = new DelegateCommand<EditStudentControl>((e) =>
					{
						if (!e.IsValid)
						{
							MessageBox.Show(e.ValidationErrors, "Ошибка валидации", MessageBoxButton.OK, MessageBoxImage.Error);
							return;
						}
						var model = e.DataContext as StudentViewModel;
						var newId = (Workspaces.Count > 0) ? Workspaces.Max(o => o.Student.Id) + 1 : 0;						
						Workspaces.Add(new StudentViewModel(new Model.Student()
						{
							Age = model.Age,
							FirstName = model.FirstName,
							Last = model.Last,
							Gender = model.Gender,
							Id = newId
						}));						
					});
				}
				return _addItemCommand;
			}
		}

		private DelegateCommand _deleteItemCommand;
		public ICommand DeleteItemCommand
		{
			get
			{
				if (_deleteItemCommand == null)
				{
					_deleteItemCommand = new DelegateCommand(() =>
					{
						var result = MessageBox.Show("Вы уверены в этом?", "Удаление", MessageBoxButton.YesNo, MessageBoxImage.Warning);
						if (result == MessageBoxResult.Yes)
						{
							var forRemove = Workspaces.Remove(o => o.IsChecked == true);
						}
					});
				}
				return _deleteItemCommand;
			}
		}


		private DelegateCommand<EditStudentControl> _editItemCommand;
		public ICommand EditItemCommand
		{
			get
			{
				if (_editItemCommand == null)
				{
					_editItemCommand = new DelegateCommand<EditStudentControl>((e) =>
					{
						if (!e.IsValid)
						{
							MessageBox.Show(e.ValidationErrors, "Ошибка валидации", MessageBoxButton.OK, MessageBoxImage.Error);
							return;
						}
						var model = e.DataContext as StudentViewModel;
						var student = Workspaces.Where(o => o.Student.Id == model.Student.Id).FirstOrDefault();
						if (student != null)
						{
							student.Age = model.Age;
							student.FirstName = model.FirstName;
							student.Last = model.Last;
							student.Gender = model.Gender;
							OnPropertyChanged("Workspaces");
						}
					});
				}
				return _editItemCommand;
			}
		}


		private DelegateCommand<StudentViewModel> _selectedItemCommand;
		public ICommand SelectedItemCommand
		{
			get
			{
				if (_selectedItemCommand == null)
				{
					_selectedItemCommand = new DelegateCommand<StudentViewModel>((e) =>
					{
						if (e != null)
						{
							EditorModel = e.Clone();
						}
					});
				}
				return _selectedItemCommand;
			}
		}

        #region MainMenu
        private DelegateCommand<MainWindowView> _exitCommand;
		public DelegateCommand<MainWindowView> ExitCommand
		{
			get
			{
				if (_exitCommand == null)
				{
					_exitCommand = new DelegateCommand<MainWindowView>((window) => {
						if (IsLoaded == true)
						{
							var mbox = MessageBox.Show("Все несохраненные данные будут потеряны. Продолжить выход?", "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Warning);
							if (mbox == MessageBoxResult.Yes)
							{
								window.Dispatcher.Invoke(() => { window.Close(); });
							}
						}
						else
						{
							window.Dispatcher.Invoke(() => { window.Close(); });
						}				
					});
				}
				return _exitCommand;
			}
		}

		private DelegateCommand<MainWindowView> _saveFileCommand;
		public DelegateCommand<MainWindowView> SaveFileCommand
		{
			get
			{
				if (_saveFileCommand == null)
				{
					_saveFileCommand = new DelegateCommand<MainWindowView>((window) =>
					{
						var mbox = MessageBox.Show("Файл будет перезаписан. Продолжить?", "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Warning);
						if (mbox == MessageBoxResult.Yes)
						{
							SaveToFile(FileName);
						}
					});
				}
				return _saveFileCommand;
			}
		}


		private DelegateCommand<MainWindowView> _saveAsFileCommand;
		public DelegateCommand<MainWindowView> SaveAsFileCommand
		{
			get
			{
				if (_saveAsFileCommand == null)
				{
					_saveAsFileCommand = new DelegateCommand<MainWindowView>((window) => {

						SaveFileDialog fileDialog = new SaveFileDialog()
						{
							DefaultExt = ".xml",
							Filter = "XML (.xml)|*.xml"
						};
						bool? result = fileDialog.ShowDialog();
						if (result == true)
						{
							var filename = fileDialog.FileName;
							if (File.Exists(filename))
							{
								var mbox = MessageBox.Show("Файл будет перезаписан. Продолжить?", "Внимание!", MessageBoxButton.YesNo, MessageBoxImage.Warning);
								if (mbox == MessageBoxResult.Yes)
								{
									SaveToFile(filename);
								}
							}
							else
							{
								SaveToFile(filename);
								OpenFromFile(filename);
							}
						}

					
					});
				}
				return _saveAsFileCommand;
			}
		}
	

		private DelegateCommand _openXmlFileCommand;
		public ICommand OpenXmlFileCommand
		{
			get
			{
				if (_openXmlFileCommand == null)
				{
					_openXmlFileCommand = new DelegateCommand(() =>
					{
						OpenFileDialog fileDialog = new OpenFileDialog()
						{
							DefaultExt = ".xml",
							Filter = "XML (.xml)|*.xml"
						};
						bool? result = fileDialog.ShowDialog();
						if (result == true)
						{
							OpenFromFile(fileDialog.FileName);
						}
					});
				}
				return _openXmlFileCommand;
			}
		}

		private void OpenFromFile(string fileName)
		{
			try
			{
				using (var reader = XmlReader.Create(fileName))
				{
					XmlSerializer serializer = new XmlSerializer(typeof(Model.Students));
					var folding = (Model.Students)serializer.Deserialize(reader);
					Workspaces = new ObservableCollection<StudentViewModel>(folding.Student.Select(b => new StudentViewModel(b)));
				}
				FileName = fileName;
				IsLoaded = true;
			}
			catch (Exception)
			{
				MessageBox.Show("Файл не распознается!", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}
        private void SaveToFile(string fileName)
        {
            try
            {
                using (var writer = XmlWriter.Create(fileName))
                {

                    var forSave = Workspaces.Select(o => o.Student).ToList();
                    var students = new Model.Students()
                    {
                        Student = forSave
                    };

                    XmlSerializer serializer = new XmlSerializer(typeof(Model.Students));
                    serializer.Serialize(writer, students);
                    MessageBox.Show($"Сохранено в {fileName}", "Сохранение");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Файл не может быть сохранен!", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion



        public MainViewModel()
		{
			IsLoaded = false;
		}
	}
}

﻿using Microsoft.Practices.Prism.Commands;
using System.Windows;
using System.Windows.Input;

namespace StudentEditor.ViewModel
{
    public class EditModelView : StudentViewModel
	{
		private string _windowTitle;
		private StudentViewModel item;
        private DelegateCommand<Window> closeWindowCommand;

		public string WindowTitle
		{
			get => _windowTitle;
			set
			{
				_windowTitle = value;
				OnPropertyChanged("WindowTitle");

			}
		}
        
		public ICommand CloseWindowCommand
		{
			get
			{
				if (closeWindowCommand == null)
				{
					closeWindowCommand = new DelegateCommand<Window>((e) =>
					{
						e?.Dispatcher.Invoke(() => { e.Close(); });
					});
				}
				return closeWindowCommand;
			}
        }

        public EditModelView(StudentViewModel item) : base(item.Student)
        {
            this.item = item;
        }
    }
}

﻿using StudentEditor.Model;

namespace StudentEditor.ViewModel
{
	public class StudentViewModel : ViewModelBase
	{
		private bool _isChecked;
		public Student Student { get; set; }
        
		public StudentViewModel(Student student)
		{
			this.Student = student;
		}

		public string FirstName
		{
            get => Student.FirstName;
            set
			{
				Student.FirstName = value;
				OnPropertyChanged("FirstName");
				OnPropertyChanged("FullName");
			}
		}

		public string Last
		{
            get => Student.Last;
            set
			{
				Student.Last = value;
				OnPropertyChanged("Last");
				OnPropertyChanged("FullName");
			}
		}

		public string FullName => Student.FirstName + " " + Student.Last; 

		public int Gender
		{
            get => Student.Gender;
            set
			{
				Student.Gender = value;
				OnPropertyChanged("Gender");
			}
		}
		public int Age
		{
            get => Student.Age;
            set
			{
				Student.Age = value;
				OnPropertyChanged("Age");
			}
		}

		public bool IsChecked
		{
            get => _isChecked;
            set
			{
				_isChecked = value;
				OnPropertyChanged("IsChecked");
			}
		}

		public StudentViewModel Clone()
		{
			return new StudentViewModel(new Student() { Age = Age, FirstName = FirstName, Gender = Gender, Id = Student.Id, Last = Last });
		}
	}
}

﻿using StudentEditor.ViewModel;
using System.Windows;

namespace StudentEditor
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
	{
		private void OnStartup(object sender, StartupEventArgs e)
		{
			MainViewModel viewModel = new MainViewModel();
			MainWindowView mainWindow = new MainWindowView()
			{
				DataContext = viewModel
			};
			mainWindow.Show();
		}
	}
}

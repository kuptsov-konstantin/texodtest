﻿using System.Xml.Serialization;
namespace StudentEditor.Model
{

	[XmlRoot(ElementName = "Student")]
	public class Student
	{
		[XmlElement(ElementName = "FirstName")]
		public string FirstName { get; set; }
		[XmlElement(ElementName = "Last")]
		public string Last { get; set; }
		[XmlElement(ElementName = "Age")]
		public int Age { get; set; }
		[XmlElement(ElementName = "Gender")]
		public int Gender { get; set; }
		[XmlAttribute(AttributeName = "Id")]
		public int Id { get; set; }		
	}
}


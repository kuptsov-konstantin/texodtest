﻿using System.Collections.Generic;
using System.Xml.Serialization;
namespace StudentEditor.Model
{
    [XmlRoot(ElementName = "Students")]
	public class Students
	{
		[XmlElement(ElementName = "Student")]
		public List<Student> Student { get; set; }
	}
}

